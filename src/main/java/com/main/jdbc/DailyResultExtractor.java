package com.main.jdbc;

import com.main.domain.DailyResult;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by patrykw on 24.04.16.
 */
public class DailyResultExtractor implements ResultSetExtractor<List<DailyResult>> {
	@Override
	public List<DailyResult> extractData(ResultSet resultSet) throws SQLException, DataAccessException {
		List<DailyResult> results = new ArrayList<DailyResult>();

		while(resultSet.next()) {
			DailyResult result = new DailyResult();
			result.setResultId(resultSet.getInt(1));
			result.setValue(resultSet.getBigDecimal(3));
			result.setDate(resultSet.getDate(2));
		}

		return results;
	}
}
