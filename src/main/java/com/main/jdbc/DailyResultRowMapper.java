package com.main.jdbc;

import com.main.domain.DailyResult;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by patrykw on 24.04.16.
 */
public class DailyResultRowMapper implements RowMapper<List<DailyResult>> {

	@Override
	public List<DailyResult> mapRow(ResultSet resultSet, int i) throws SQLException {
		List<DailyResult> results = new ArrayList<DailyResult>();

		while (resultSet.next()) {
			DailyResult result = new DailyResult();
			result.setResultId(resultSet.getInt(1));
			result.setValue(resultSet.getBigDecimal(3));
			result.setDate(resultSet.getDate(2));
			results.add(result);
		}

		return results;
	}
}
