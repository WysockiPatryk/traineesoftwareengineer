package com.main.editors;

import java.beans.PropertyEditorSupport;
import java.math.BigDecimal;

/**
 * Created by patrykw on 24.04.16.
 */
public class BigDecimalEditor extends PropertyEditorSupport {

	@Override
	public void setAsText(String text) {
		BigDecimal moneyValue = new BigDecimal(text);
		this.setValue(moneyValue);
	}
	@Override
	public String getAsText() {
		BigDecimal moneyValue = (BigDecimal)this.getValue();
		return moneyValue.toString();
	}

}
