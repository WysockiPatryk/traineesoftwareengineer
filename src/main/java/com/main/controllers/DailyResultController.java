package com.main.controllers;

import com.main.domain.Results;
import com.main.domain.DailyResult;
import com.main.domain.TimePeriod;
import com.main.editors.BigDecimalEditor;
import com.main.services.DailyResultService;
import com.main.views.Views;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.cloud.cloudfoundry.com.fasterxml.jackson.annotation.JsonView;
import org.springframework.cloud.cloudfoundry.com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.view.RedirectView;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.sql.Time;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

/**
 * Created by patrykw on 23.04.16.
 */

@Controller
public class DailyResultController {

	@Autowired
	DailyResultService dailyResultService;

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		dateFormat.setLenient(false);
		binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
		binder.registerCustomEditor(BigDecimal.class, new BigDecimalEditor());
	}

	@RequestMapping("/")
	public ModelAndView displayResults() {
		ModelAndView modView = new ModelAndView("HelloWeb");
		return modView;
	}

	@RequestMapping("/compare")
	public ModelAndView displayCompare() {
		ModelAndView modView = new ModelAndView("Compare");
		return modView;
	}

	@RequestMapping(value="/getPeriod", method = RequestMethod.POST)
	public
	@ResponseBody
	List<DailyResult> getSearchResultViaAjax(@RequestBody String json) throws ParseException, IOException {

		List<DailyResult> results;

		ObjectMapper mapper = new ObjectMapper();
		TimePeriod period = mapper.readValue(json, TimePeriod.class);

		DateFormat format = new SimpleDateFormat("dd-MM-yyyy");
		Date dateFrom = format.parse(period.getFrom());
		Date dateTo = format.parse(period.getTo());

		results = dailyResultService.getResultsListFromToDate(dateFrom, dateTo);

//		String responseString = "{\"results\" : [";
//
//		Iterator<DailyResult> itr = results.iterator();
//		if(itr.hasNext()) {
//			do {
//				DailyResult res = itr.next();
//				responseString += res.toString();
//				if(itr.hasNext()) {
//					responseString += ", ";
//				}
//			}while(itr.hasNext());
//		}
//
//
//		responseString += "]}";

		return results;

	}
}
