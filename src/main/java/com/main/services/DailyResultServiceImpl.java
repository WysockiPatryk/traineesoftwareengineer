package com.main.services;

import com.main.dao.DailyResultDao;
import com.main.domain.DailyResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * Created by patrykw on 24.04.16.
 */
@Service
public class DailyResultServiceImpl implements DailyResultService {

	@Autowired
	DailyResultDao resultDao;

	@Override
	public List<DailyResult> getResultsList() {
		return resultDao.getResultsList();
	}

	@Override
	public List<DailyResult> getResultsListFromToId(int fromID, int toID) {
		return resultDao.getResultsListFromToId(fromID, toID);
	}

	@Override
	public List<DailyResult> getResultsListFromToDate(Date dateFrom, Date dateTo) {
		return resultDao.getResultListFromToDate(dateFrom, dateTo);
	}

	@Override
	public void insertData(DailyResult result) {

	}

	@Override
	public void updateData(DailyResult result) {

	}

	@Override
	public void deleteData(DailyResult result) {

	}

	@Override
	public void getDailyResult(String id) {

	}
}
