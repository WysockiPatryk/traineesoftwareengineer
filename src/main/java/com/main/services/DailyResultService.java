package com.main.services;

import com.main.domain.DailyResult;

import java.util.Date;
import java.util.List;

/**
 * Created by patrykw on 24.04.16.
 */
public interface DailyResultService {

	List<DailyResult> getResultsList();
	List<DailyResult> getResultsListFromToId(int fromID, int toID);
	List<DailyResult> getResultsListFromToDate(Date dateFrom, Date dateTo);
	void insertData(DailyResult result);
	void updateData(DailyResult result);
	void deleteData(DailyResult result);
	void getDailyResult(String id);
}
