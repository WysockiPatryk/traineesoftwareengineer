package com.main.domain;

import java.sql.Time;
import java.util.Date;

/**
 * Created by patrykw on 28.04.16.
 */
public class TimePeriod {
	String from;
	String to;

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}
}
