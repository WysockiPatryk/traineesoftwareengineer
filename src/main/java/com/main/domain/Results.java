package com.main.domain;

import com.main.views.Views;
import org.springframework.cloud.cloudfoundry.com.fasterxml.jackson.annotation.JsonView;

import java.util.List;

/**
 * Created by patrykw on 28.04.16.
 */
public class Results {
	@JsonView(Views.Public.class)
	List<DailyResult> results;
}
