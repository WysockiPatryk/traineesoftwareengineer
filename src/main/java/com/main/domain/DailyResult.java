package com.main.domain;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.main.CustomSerializer.CustomDateSerializer;

import java.math.BigDecimal;
import java.util.Date;

/**
 * Created by patrykw on 24.04.16.
 */
public class DailyResult {

	private int resultId;
	private Date date;
	private BigDecimal value;

	public int getResultId() {
		return resultId;
	}

	public void setResultId(int resultId) {
		this.resultId = resultId;
	}

	@JsonSerialize(using = CustomDateSerializer.class)
	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public BigDecimal getValue() {
		return value;
	}

	public void setValue(BigDecimal value) {
		this.value = value;
	}

	@Override
	public String toString() {
		return "{\"resultId\" : " + resultId + ", \"date\" : \"" + date.toString() + "\", \"value\" : " + value + "}";
	}
}
