package com.main.dao;

import com.main.domain.DailyResult;
import com.main.jdbc.DailyResultRowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by patrykw on 24.04.16.
 */
public class DailyResultDaoImpl implements DailyResultDao {

	class ResponseList extends ArrayList<DailyResult> {}

	@Autowired
	DataSource dataSource;

	String databaseName = "ad_712af443dc33be2.";

	@Override
	public List<DailyResult> getResultsList() {

		String sql = "select * from " + databaseName + "dailyResults";

		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);

		return convertFromRows(rows);
	}
	@Override
	public List<DailyResult> getResultsListFromToId(int fromID, int toID) {
		String sql = "select * from " + databaseName + "dailyResults where resultId >= " + fromID + " AND resultId < " + toID;

		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);

		return convertFromRows(rows);
	}
	@Override
	public List<DailyResult> getResultListFromToDate(Date dateFrom, Date dateTo) {

		java.sql.Date fromSql = new java.sql.Date(dateFrom.getTime());
		java.sql.Date toSql = new java.sql.Date(dateTo.getTime());

		String sql = "SELECT * from " + databaseName + "dailyResults WHERE date >= '" +
				fromSql + "' AND date < '" +
				toSql + "'";

		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		List<Map<String, Object>> rows = jdbcTemplate.queryForList(sql);

		return convertFromRows(rows);
	}
	@Override
	public void insertData(DailyResult result) {
		throw new UnsupportedOperationException("not yet implemented");
	}

	@Override
	public void updateData(DailyResult result) {
		throw new UnsupportedOperationException("not yet implemented");
	}

	@Override
	public void deleteData(DailyResult result) {
		throw new UnsupportedOperationException("not yet implemented");
	}

	@Override
	public void getDailyResult(String id) {
		throw new UnsupportedOperationException("not yet implemented");
	}

	private List<DailyResult> convertFromRows(List<Map<String, Object>> rows) {
		List<DailyResult> userList = new ResponseList();
		for(Map row : rows) {
			DailyResult result = new DailyResult();
			result.setResultId((int)row.get("resultId"));
			result.setDate((Date)row.get("date"));
			result.setValue((BigDecimal)row.get("value"));
			userList.add(result);
		}
		return userList;
	}
}
