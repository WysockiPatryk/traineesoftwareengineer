/**
 * Created by patrykw on 29.04.16.
 */

jQuery(document).ready(function($) {

    $("#from").datepicker({
        dateFormat : "dd-MM-yy",
        monthNames : [ "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12" ]
    });
    $("#to").datepicker({
        dateFormat : "dd-MM-yy",
        monthNames : [ "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12" ]
    });
});


function reactToAjax(results) {
    reactToData(JSON.parse(results));
}

function reactToData(data) {
    var arr = [];

    $.each(data, function(indexInArray, value ) {
        arr.push($.map(value, function(val) {return val;}));
    });

    if ( $.fn.dataTable.isDataTable( '#resultsTable' ) ) {
        table = $('#resultsTable').DataTable();
        table.destroy()
    }
    else {

    }

    $('#resultsTable').DataTable( {
        retrieve: true,
        bFilter: false,
        data: arr,
        columns: [
            { title: "id" },
            { title: "date" },
            { title: "value" }
        ]
    });

    drawChart(arr);
}

function drawChart(data) {
    $("#chartdiv").empty();
    var line1 = [];
    console.log(data);
    var line2 = calcuateDefaultGrowth(data[0][1], data[data.length - 1][1]);
    $.each(data, function(index, res) {
        var element = [];
        element.push(res[1] + " 00:00PM");
        element.push(res[2]);
        line1.push(element);
    });
    console.log("Wykres");
    console.log(line1);
    console.log(line2);
    $.jqplot('chartdiv', [line1, line2], {
        title:'Comparison',
        axes:{
            xaxis:{
                renderer : $.jqplot.DateAxisRenderer,
                tickOptions:{
                    formatString:'%b&nbsp;%#d'
                }
            },
            yaxis:{
                tickOptions:{
                    formatString:'%.2f'
                }
            }
        },
        highlighter: {
            show: true,
            sizeAdjust: 7.5
        },
        cursor: {
            show: false
        },
        series:[{lineWidth:2}]
    });
}
function calcuateDefaultGrowth(to, from) {
    var fromDate = new Date(from);
    var toDate = new Date(to);

    console.log(from);
    console.log(to);
    console.log(fromDate);
    console.log(toDate);

    var results = [];
    var startAmmount = $("#startAmmount").val();
    var intrest = $("#intrest").val();
    var dateString;
    var finalDate = toDate.setMonth((toDate.getMonth()+6));
    console.log(finalDate.toString());
    do {
        dateString = "";
        dateString = fromDate.toISOString();
        dateString = dateString.slice(0, 10);
        dateString += ' 0:00PM';
        results.push([dateString, startAmmount]);
        fromDate.setMonth(fromDate.getMonth() + 1);
        startAmmount = startAmmount * (1 + (intrest/12));
    } while (fromDate < finalDate);
    return results;
}