/**
 * Created by patrykw on 29.04.16.
 */



jQuery(document).ready(function($) {

    $("#from").datepicker({
        dateFormat : "dd-MM-yy",
        monthNames : [ "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12" ]
    });
    $("#to").datepicker({
        dateFormat : "dd-MM-yy",
        monthNames : [ "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12" ]
    });
});


function reactToAjax(results) {
    var arr = JSON.parse(results);
    reactToData(arr);
}

function reactToData(data) {

    var arr = [];

    $.each(data, function(indexInArray, value ) {
        arr.push($.map(value, function(val) {return val;}));
    });

    if ( $.fn.dataTable.isDataTable( '#resultsTable' ) ) {
        table = $('#resultsTable').DataTable();
        table.destroy()
    }
    else {

    }

    $('#resultsTable').DataTable( {
        retrieve: true,
        bFilter: false,
        data: arr,
        columns: [
            { title: "id" },
            { title: "date" },
            { title: "value" }
        ]
    });
    drawChart(arr);
}

function drawChart(data) {
    $("#chartdiv").empty();
    var line1 = [];
    $.each(data, function(index, res) {
        var element = [];
        element.push(res[1] + " 00:00PM");
        element.push(res[2]);
        line1.push(element);
    });

    console.log("Wykres");
    $.jqplot('chartdiv', [line1], {
        title:'Comparison',
        axes:{
            xaxis:{
                renderer : $.jqplot.DateAxisRenderer,
                tickOptions:{
                    formatString:'%b&nbsp;%#d'
                }
            },
            yaxis:{
                tickOptions:{
                    formatString:'%.2f'
                }
            }
        },
        highlighter: {
            show: true,
            sizeAdjust: 7.5
        },
        cursor: {
            show: false
        },
        series:[{lineWidth:2}]
    });
}