
function sendDates(callback) {
    var data = {};

    data["from"] = $("#from").val();
    data["to"] = $("#to").val();

    console.log(data);

    $.ajax({
        'type' : "POST",
        'contentType' : "application/json",
        'url' : "getPeriod",
        'data' : JSON.stringify(data),
        'dataType' : 'text',
        'timeout' : 5000,
        success : callback,
        error : function(error) {
            console.log("error");
            console.log(error);
        },
        done : function(msg) {
            console.log(msg);
            console.log("done");
        }
    })
};

