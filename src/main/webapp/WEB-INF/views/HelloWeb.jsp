<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html;charset=utf-8" >
    <title>Display</title>

    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="Wysocki Patryk">

    <c:url var="jq" value="/resources/theme/js/jquery-2.2.3.min.js"/>
    <c:url var="Ajax" value="resources/theme/js/page_scripts/AjaxWorkings.js"/>
    <c:url var="jqplot" value="/resources/theme/js/jquery.jqplot.min.js"/>
    <c:url var="exc" value="/resources/theme/js/excanvas.js"/>
    <c:url var="dateRen" value="/resources/theme/js/plugins/jqplot.dateAxisRenderer.js"/>
    <c:url var="highliter" value="/resources/theme/js/plugins/jqplot.highlighter.js"/>
    <c:url var="cursor" value="/resources/theme/js/plugins/jqplot.cursor.js"/>
    <c:url var="jqcss" value="/resources/theme/css/jquery.jqplot.min.css"/>
    <c:url var="jqui" value="/resources/theme/js/jquery-ui-1.9.2.custom.min.js"/>
    <c:url var="css" value="/resources/theme/css/main.css"/>
    <c:url var="bootstrapCss" value="/resources/theme/css/bootstrap.min.css"/>
    <c:url var="bootstrapResponsiveCss" value="/resources/theme/css/bootstrap-responsive.min.css"/>
    <c:url var="uiCss" value="/resources/theme/css/datepicker.css"/>
    <c:url var="homeScript" value="/resources/theme/js/page_scripts/HomeSite.js"/>
    <c:url var="dataTables" value="/resources/theme/js/jquery.dataTables.min.js"/>
    <c:url var="validationJquery" value="/resources/theme/js/jquery.validate.min.js"/>
    <c:url var="dataTablesBootstrap" value="/resources/theme/js/dataTables.bootstrap.min.js"/>
    <c:url var = "bootstrap" value = "/resources/theme/js/bootstrap.js"/>
    <c:url var = "defaultBootstrapCss" value="/resources/theme/css/bootstrap-theme.min.css"/>
    <c:url var="defaultDatatablesBootstrapCss" value="/resources/theme/css/dataTables.bootstrap.min.css"/>
    <c:url var="datatablesCss" value="/resources/theme/css/jquery.dataTables.min.css"/>

    <link rel="stylesheet" type="text/css" href="${defaultBootstrapCss}"/>
    <link rel="stylesheet" type="text/css" href="${bootstrapCss}"/>
    <link rel="stylesheet" type="text/css" href="${bootstrapResponsiveCss}"/>
    <link rel="stylesheet" type="text/css" href="${jqcss}" />
    <link rel="stylesheet" type="text/css" href="${uiCss}"/>
    <link rel="stylesheet" type="text/css" href="${defaultDatatablesBootstrapCss}"/>
    <link rel="stylesheet" type="text/css" href="${datatablesCss}"/>

</head>
<body>

<nav class="navbar navbar-inverse">
    <div class="container">
        <ul class="nav navbar-nav">
            <li class="active"><a href="/">Display</a></li>
            <li><a href="/compare">Compare</a></li>
        </ul>
    </div>
</nav>

<div class="container">
    <form id="compareForm">
        <label for="from">Search from:</label>
        <input type="text" name="from" id="from" readonly="readonly">
        <label for="to">Search to:</label>
        <input type="text" name="to" id="to" readonly="readonly"> <br>
        <input class="btn-primary" type="submit" value="Submit" id="submit">
    </form>
</div>

<div class="container">
    <div id="chartdiv" style="height:400px;width:100%; "></div>
</div>

<div class="container">
    <table class="table table-striped table-bordered" id="resultsTable">

    </table>
</div>

<script type="text/javascript" src="${jq}"></script>
<script type="text/javascript" src="${Ajax}"></script>
<script type="text/javascript" src="${jqplot}"></script>
<script type="text/javascript" src="${exc}"></script>
<script type="text/javascript" src="${dateRen}"></script>
<script type="text/javascript" src="${validationJquery}"></script>
<script type="text/javascript" src="${highliter}"></script>
<script type="text/javascript" src="${jqui}"></script>
<script type="text/javascript" src="${cursor}"></script>
<script type="text/javascript" src="${bootstrap}"></script>
<script type="text/javascript" src="${homeScript}"></script>
<script type="text/javascript" src="${dataTables}"></script>
<script type="text/javascript" src="${dataTablesBootstrap}"></script>

<script>
    $.validator.setDefaults({
        submitHandler : function() {
            sendDates(reactToAjax);
        }
    });
    $("#compareForm").validate({
        debug : true,
        rules : {
            from : {
                required : true
            },
            to : {
                required : true
            }
        }
    });
</script>

</body>
</html>